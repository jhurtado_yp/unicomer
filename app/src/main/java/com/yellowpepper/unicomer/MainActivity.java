package com.yellowpepper.unicomer;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.yellowpepper.unicomer.network.NetworkCallHelper;
import com.yellowpepper.unicomer.ui.fragments.MainFragment;
import com.yellowpepper.unicomer.utils.RestInvoker;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements RestInvoker.RestProcessor{

    private static Context appContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appContext = this;

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, new MainFragment());
        fragmentTransaction.commit();
    }

    public static Context getContext(){
        return appContext;
    }


    @Override
    public void callRestAPI(final RestInvoker invoker, final String requestTag, String url, JSONObject jsonRequest, int requestMethod) {

        NetworkCallHelper.getInstance().createStringRequest(requestMethod, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    invoker.receiveServerResponse(requestTag, response);
                } catch (JSONException e) {
                    Log.e(invoker.getClass().getSimpleName(), e.getMessage(), e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(invoker.getClass().getSimpleName(), error.getMessage(), error);
            }
        });
    }
}
