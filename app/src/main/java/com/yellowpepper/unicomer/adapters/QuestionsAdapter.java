package com.yellowpepper.unicomer.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.yellowpepper.unicomer.model.Question;
import com.yellowpepper.unicomer.R;
import com.yellowpepper.unicomer.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juang on 2/22/2018.
 */

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionViewHolder> {

    private List<Question> questionList = new ArrayList<>();
    private QuestionClickListener listener;
    private static final String TAG = QuestionsAdapter.class.getSimpleName();

    public void setQuestionList(List<Question> list){
        this.questionList = list;
    }
    public void setOnClickListener(QuestionClickListener listener){
        this.listener = listener;
    }
    public List<Question> getQuestionList(){
        return questionList;
    }


    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout questionLine = null;
        switch(viewType) {
            case 1:
                questionLine = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_text_question, parent, false);
                break;
            case 2:
                questionLine = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_radio_question, parent, false);
                break;
            case 3: case 4:
               questionLine = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_default_question, parent, false);
               break;

        }
        QuestionViewHolder questionViewHolder = new QuestionViewHolder(questionLine,viewType);
        return questionViewHolder;
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {
        Question question = questionList.get(position);
        switch(holder.viewType) {
            case 1:
                Log.i(TAG, "starting to draw TEXT question  --  millis started: " + System.currentTimeMillis());
                Question.TextQuestion textQuestion = (Question.TextQuestion) question.getObject();
                TextView textView = holder.holderLine.findViewById(R.id.question_text);
                textView.setText((position + 1) + " - " + textQuestion.value);
                EditText editText = holder.holderLine.findViewById(R.id.question_edit);
                textQuestion.setAnswerEdit(editText);
                if (textQuestion.format.equals(Constants.TEXT)) {
                    editText.setHint(textView.getContext().getString(R.string.write_your_answer));
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(textQuestion.lenght)});
                } else if (textQuestion.format.equals(Constants.DATE)){
                    editText.setHint(Constants.DATE_FORMAT.toUpperCase());
                    editText.setRawInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
                }
                else if(textQuestion.format.equals(Constants.NUMBER)) {
                    editText.setHint(editText.getContext().getString(R.string.write_amount));
                    editText.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(textQuestion.lenght)});
                }
                Log.i(TAG,"finished drawing TEXT question  --  millis finished: "+System.currentTimeMillis());
                break;
            case 2:
                Log.i(TAG,"starting to draw RADIO question  --  millis started: "+System.currentTimeMillis());
                Question.RadioQuestion radioQuestion = (Question.RadioQuestion) question.getObject();
                TextView radioTextView = holder.holderLine.findViewById(R.id.question_text);
                radioTextView.setText((position+1)+" - " +radioQuestion.text);
                RadioGroup radioGroup = holder.holderLine.findViewById(R.id.question_radio);
                for(int i=0; i< radioQuestion.optionList.size();i++){
                    Question.RadioOption radioOption = radioQuestion.optionList.get(i);
                    RadioButton radioButton = new RadioButton(radioGroup.getContext());
                    radioButton.setText(radioOption.value);
                    radioGroup.addView(radioButton);
                    radioOption.setRadioButton(radioButton);
                }
                Log.i(TAG,"finished drawing RADIO question  --  millis finished: "+System.currentTimeMillis());
                break;
            case 3:
                Log.i(TAG,"starting to draw FILE question  --  millis started: "+System.currentTimeMillis());
                final Question.FileQuestion fileQuestion = (Question.FileQuestion) question.getObject();
                LinearLayout resultLine = holder.holderLine.findViewById(R.id.result_line);
                fileQuestion.setResultLine(resultLine);
                TextView fileTextView = holder.holderLine.findViewById(R.id.question_text);
                fileTextView.setText((position+1)+" - " +fileQuestion.title);
                Button fileButton = holder.holderLine.findViewById(R.id.question_button);
                fileButton.setText("Pick File");
                fileButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onFileButtonClicked(fileQuestion);
                    }
                });
                Log.i(TAG,"finished drawing FILE question  --  millis finished: "+System.currentTimeMillis());

                break;
            case 4:
                Log.i(TAG,"starting to draw CONTACT question  --  millis started: "+System.currentTimeMillis());
                final Question.ContactQuestion contactQuestion = (Question.ContactQuestion) question.getObject();
                LinearLayout resultLine1 = holder.holderLine.findViewById(R.id.result_line);
                contactQuestion.setResultLine(resultLine1);
                TextView contactTextView = holder.holderLine.findViewById(R.id.question_text);
                contactTextView.setText((position+1)+" - " +contactQuestion.title);
                Button contactButton = holder.holderLine.findViewById(R.id.question_button);
                contactButton.setText("Pick Contacts");
                contactButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onContactButtonClicked(contactQuestion);
                    }
                });
                Log.i(TAG,"finished drawing CONTACT question  --  millis finished: "+System.currentTimeMillis());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    @Override
    public int getItemViewType(int position) {
        String type = questionList.get(position).getType();
        if(type.equals(Constants.TEXT))
            return 1;
        else if(type.equals(Constants.RADIO))
            return 2;
        else if(type.equals(Constants.FILE))
            return 3;
        else if(type.equals(Constants.CONTACT))
            return 4;
        else
            return 0;

    }

    public class QuestionViewHolder extends RecyclerView.ViewHolder{

        LinearLayout holderLine;
        int viewType;
        public QuestionViewHolder(View itemView, int type) {
            super(itemView);
            holderLine = (LinearLayout) itemView;
            viewType = type;

        }
    }

    public interface QuestionClickListener{
        void onFileButtonClicked(Question.FileQuestion fileQuestion);
        void onContactButtonClicked(Question.ContactQuestion contactQuestion);
    }
}
