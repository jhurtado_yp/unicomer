package com.yellowpepper.unicomer.model;

import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.yellowpepper.unicomer.R;
import com.yellowpepper.unicomer.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by juang on 2/22/2018.
 */

public class Question implements Serializable {

    private String type;
    private Object object;
    private int id;

    public Question(String type, Object object, int id){
        this.type = type;
        this.object = object;
        this.id=id;
    }

    public String getType(){
        return type;
    }

    public Object getObject(){
        return object;
    }

    public String getId(){
        return id+"";
    }

    public static class TextQuestion{
        public int lenght;
        public String value;
        public String format;
        private EditText answerEdit;

        public TextQuestion(JSONObject jsonObject) throws JSONException {
            this.lenght = jsonObject.getInt(Constants.LENGHT);
            this.value = jsonObject.getString(Constants.VALUE);
            this.format = jsonObject.getString(Constants.FORMAT);
        }
        public void setAnswerEdit(EditText editText){
            this.answerEdit = editText;
        }

        public String getAnswer(){
            String answer = answerEdit.getText().toString();
            if(format.equals(Constants.NUMBER)){
                if(!answer.matches(Constants.NUMERIC_REGEX)) {
                    answerEdit.setError(answerEdit.getContext().getString(R.string.error_must_be_number));

                    return null;
                }
            }else if(format.equals(Constants.DATE)){
                if(isDateValid(answer))
                    return answer;
                else{
                    answerEdit.setError(String.format(answerEdit.getContext().getString(R.string.error_wrong_format),Constants.DATE_FORMAT.toUpperCase()));
                    return null;
                }
            }

            return answer;
        }

        public boolean hasError(){
            if(answerEdit.getError() != null){
                return true;
            }
            return false;
        }

        private boolean isDateValid(String date){
            SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
            formatter.setLenient(false);
            try{
                Log.i(Question.class.getSimpleName(),"Date has the right format");
                formatter.parse(date);
                return true;
            } catch (ParseException e) {
                Log.e(Question.class.getSimpleName(),"Date has a wrong format");
                return false;
            }
        }
    }

    public static class RadioQuestion{
        public String text;
        public List<RadioOption> optionList = new ArrayList<>();

        public RadioQuestion(JSONObject jsonObject) throws JSONException {
            this.text = jsonObject.getString(Constants.VALUE);
            JSONArray jsonArray = jsonObject.getJSONArray(Constants.OPTIONS);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject optionObject = jsonArray.getJSONObject(i);
                RadioOption option = new RadioOption(optionObject);
                optionList.add(option);
            }
        }

    }

    public static class FileQuestion{
        public String fileFormat;
        public String title;
        public String url;
        public int size;
        private String filePath;
        private LinearLayout resultLine;

        public FileQuestion(JSONObject jsonObject) throws JSONException {
            this.fileFormat = jsonObject.getString(Constants.FORMAT);
            this.title = jsonObject.getString(Constants.VALUE);
            this.url = jsonObject.getString(Constants.URL);
            this.size = jsonObject.getInt(Constants.SIZE);
        }

        public void setFilePath(String filePath){
            this.filePath = filePath;
        }

        public String getFilePath(){
            return filePath;
        }

        public void setResultLine(LinearLayout line){
            this.resultLine = line;
        }

        public LinearLayout getResultLine(){
            return resultLine;
        }
    }

    public static class ContactQuestion{

        public String title;
        public int lenght;
        private List<Contact> contactList = new ArrayList<>();
        private LinearLayout contactResultLine;

        public ContactQuestion(JSONObject jsonObject) throws JSONException {
            this.title = jsonObject.getString(Constants.VALUE);
            this.lenght = jsonObject.getInt(Constants.LENGHT);
        }

        public void addContact(Contact contact){
            this.contactList.add(contact);
        }

        public void setResultLine(LinearLayout line){
            this.contactResultLine = line;
        }

        public LinearLayout getResultLine(){
            return contactResultLine;
        }

        public List<Contact> getContactList(){
            return contactList;
        }
    }

    public static class RadioOption{
        public String value ;
        private RadioButton radioButton;
        private int id;

        public RadioOption(JSONObject jsonObject) throws JSONException {
            this.value = jsonObject.getString(Constants.VALUE);
            this.id = jsonObject.getInt(Constants.ID);
        }

        public void setRadioButton(RadioButton radioButton){
            this.radioButton = radioButton;
        }

       public boolean isSelected(){
            return radioButton.isChecked();
       }

       public int getId(){
           return id;
       }

    }

    public static Question createQuestion(JSONObject jsonObject) throws JSONException {
        String type = jsonObject.getString(Constants.TYPE);
        JSONObject object = jsonObject.getJSONObject(Constants.OBJECT);
        int id = jsonObject.getInt(Constants.ID);
        Object response = null;

        if(type.equals(Constants.TEXT)){
            TextQuestion textQuestion = new TextQuestion(object);
            response = textQuestion;
        }else if(type.equals(Constants.RADIO)){
            RadioQuestion radioQuestion = new RadioQuestion(object);
            response = radioQuestion;
        }else if(type.equals(Constants.FILE)){
            FileQuestion fileQuestion = new FileQuestion(object);
            response = fileQuestion;
        }else if(type.equals(Constants.CONTACT)){
            ContactQuestion contactQuestion = new ContactQuestion(object);
            response = contactQuestion;
        }

        return new Question(type,response,id);
    }
}
