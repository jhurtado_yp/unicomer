package com.yellowpepper.unicomer.model;

import com.yellowpepper.unicomer.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juang on 2/22/2018.
 */

public class ServerResponse {

    List<Question> questionList = new ArrayList<>();

    public ServerResponse(JSONObject jsonObject){

        try {
            JSONArray jsonArray = jsonObject.getJSONArray(Constants.QUESTIONS);
            for (int i=0; i<jsonArray.length(); i++){
                JSONObject questionObject = jsonArray.getJSONObject(i);
                Question question = Question.createQuestion(questionObject);
                questionList.add(question);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<Question> getQuestionList(){
        return questionList;
    }
}
