package com.yellowpepper.unicomer.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.yellowpepper.unicomer.MainActivity;

import org.json.JSONObject;

/**
 * Created by juang on 2/22/2018.
 */

public class NetworkCallHelper {

    private RequestQueue requestQueue;
    private static NetworkCallHelper networkCallHelper;
    private static Context appContext;
    private int maxCacheSizeInBytes = 10 * 1024 * 1024; // 10MB

    private NetworkCallHelper(Context context) {
        appContext = context;
        requestQueue = getRequestQueue();
    }

    public void createStringRequest(int method, String url, final Response.Listener<String> onSuccess,
                                    final Response.ErrorListener onError) {
        StringRequest stringRequest = new StringRequest(method, url, onSuccess, onError);
        getRequestQueue().add(stringRequest);
    }

    public static synchronized NetworkCallHelper getInstance() {
        if (networkCallHelper == null) {
            networkCallHelper = new NetworkCallHelper(MainActivity.getContext());
        }
        return networkCallHelper;
    }

    public RequestQueue getRequestQueue() {
        if (this.requestQueue == null) {
            Cache cache = new DiskBasedCache(appContext.getCacheDir(), maxCacheSizeInBytes);
            try {
                Network network = new BasicNetwork(new HurlStack(null, null));
                this.requestQueue = new RequestQueue(cache, network);
            } catch (Exception e) {
                Log.e("getRequestQueue", e.getMessage(), e);
            }
            this.requestQueue.start();
        }
        return this.requestQueue;
    }
}
