package com.yellowpepper.unicomer.ui.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.yellowpepper.unicomer.R;
import com.yellowpepper.unicomer.adapters.QuestionsAdapter;
import com.yellowpepper.unicomer.model.Contact;
import com.yellowpepper.unicomer.model.Question;
import com.yellowpepper.unicomer.model.ServerResponse;
import com.yellowpepper.unicomer.utils.Constants;
import com.yellowpepper.unicomer.utils.RESTEndpoints;
import com.yellowpepper.unicomer.utils.RestInvoker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by juang on 2/22/2018.
 */

public class MainFragment extends Fragment implements RestInvoker, QuestionsAdapter.QuestionClickListener {

   private RestInvoker.RestProcessor restProcessor;
   private final static String TAG = MainFragment.class.getSimpleName();
   private QuestionsAdapter adapter;
   private final int PICK_FILE_REQUEST_CODE=1;
   private final int PICK_CONTACT_REQUEST_CODE=2;
   private Question.FileQuestion currentFileQuestion;
   private Question.ContactQuestion currentContactQuestion;

    public final static String QUESTIONS_TAG="Question TAG";
    public final static String ANSWERS_TAG="Answers TAG";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        restProcessor = (RestInvoker.RestProcessor) getActivity();
        adapter = new QuestionsAdapter();
        getQuestions();

        return inflater.inflate(R.layout.fragment_main,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView questionsRecycler = view.findViewById(R.id.questions_recycler);
        questionsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        questionsRecycler.setAdapter(adapter);

        Button sendButton = view.findViewById(R.id.question_send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




               try {
                    JSONObject jsonObject = createJSONRequest(adapter.getQuestionList());
                    postAnswers(jsonObject);
                   boolean allAnswersRight=true;
                   List<Question> questionList = adapter.getQuestionList();
                   for(Question question : questionList){
                       if(question.getType().equals(Constants.TEXT)){
                           Question.TextQuestion textQuestion = (Question.TextQuestion) question.getObject();
                           if(textQuestion.hasError())
                               allAnswersRight = false;
                       }
                   }

                   if(allAnswersRight) {
                       Toast.makeText(getActivity(), getActivity().getString(R.string.request_created_successfuly) + jsonObject.toString(), Toast.LENGTH_LONG).show();
                       Log.i(TAG, "Request created successfuly");
                   } else {
                       Toast.makeText(getActivity(), getActivity().getString(R.string.error_with_answers), Toast.LENGTH_SHORT).show();
                       Log.e(TAG,"There was an error with the answers");
                   }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void getQuestions(){
        Log.i(TAG,"Get Questions  -- millis started: "+System.currentTimeMillis());
        //restProcessor.callRestAPI(MainFragment.this,TAG,RESTEndpoints.getInstance(getActivity()).mockQuestionsPath(),null, Request.Method.GET);
        restProcessor.callRestAPI(MainFragment.this,QUESTIONS_TAG, RESTEndpoints.getInstance(getActivity()).mockQuestionsPath(),null, Request.Method.GET);
    }

    private void postAnswers(JSONObject jsonObject){
        Log.i(TAG,"Post answers");
        restProcessor.callRestAPI(MainFragment.this,ANSWERS_TAG,RESTEndpoints.getInstance(getActivity()).mockAnswersPath(),jsonObject,Request.Method.POST);
    }

    private JSONObject createJSONRequest(List<Question> questionList) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for(int i=0;i<questionList.size();i++){
            Question question = questionList.get(i);
            if(question.getType().equals(Constants.TEXT)){
                Question.TextQuestion textQuestion = (Question.TextQuestion) question.getObject();
               jsonObject.put(question.getId(), textQuestion.getAnswer());
            }else if (question.getType().equals(Constants.RADIO)){
                Question.RadioQuestion radioQuestion = (Question.RadioQuestion) question.getObject();
                int optionSelected=0;
                for(int n=0; n<radioQuestion.optionList.size();n++){
                    Question.RadioOption radioOption = radioQuestion.optionList.get(n);
                    if(radioOption.isSelected()) {
                        optionSelected = radioOption.getId();
                        break;
                    }
                }
                jsonObject.put(question.getId(),optionSelected);

            }else if(question.getType().equals(Constants.FILE)){
                Question.FileQuestion fileQuestion = (Question.FileQuestion) question.getObject();
                jsonObject.put(question.getId(),fileQuestion.getFilePath());

            }else if(question.getType().equals(Constants.CONTACT)){
                Question.ContactQuestion contactQuestion = (Question.ContactQuestion) question.getObject();
                List<Contact> contactList = contactQuestion.getContactList();
                JSONArray contactListArray = new JSONArray();
                for(int n=0; n< contactList.size();n++){
                    JSONObject contactObject = new JSONObject();
                    Contact contact = contactList.get(n);
                    contactObject.put(contact.getName(),contact.getPhoneNumber());
                    contactListArray.put(contactObject);
                }
                jsonObject.put(question.getId(),contactListArray);

            }
        }

        return jsonObject;
    }
    @Override
    public <T> void receiveServerResponse(String requestTag, T jsonObject) throws JSONException {

        Log.i(TAG,"parse response received  --  Millis received: "+System.currentTimeMillis());
        ServerResponse serverResponse = new ServerResponse(new JSONObject((String)jsonObject));
        handleSuccessfulResponse(serverResponse);
    }

    private void handleSuccessfulResponse(ServerResponse response){
        Log.i(TAG,"preparing to draw all questions  --  millis: "+System.currentTimeMillis());
       List<Question> questionList = response.getQuestionList();
        adapter.setQuestionList(questionList);
        adapter.notifyDataSetChanged();
        adapter.setOnClickListener(MainFragment.this);
    }

    @Override
    public void onFileButtonClicked(Question.FileQuestion fileQuestion) {
        Log.i(TAG,"File button clicked");
        currentFileQuestion = fileQuestion;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent,PICK_FILE_REQUEST_CODE);
    }

    @Override
    public void onContactButtonClicked(Question.ContactQuestion contactQuestion) {
        Log.i(TAG,"Contact button clicked");
        currentContactQuestion = contactQuestion;
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent,PICK_CONTACT_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
      //  super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case PICK_FILE_REQUEST_CODE:
                String filePath = data.getDataString();
                if(currentFileQuestion!=null)
                    currentFileQuestion.setFilePath(filePath);
                TextView fileTextView = new TextView(getActivity());
                fileTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                fileTextView.setText(filePath);
                currentFileQuestion.getResultLine().removeAllViews();
                currentFileQuestion.getResultLine().addView(fileTextView);
                break;
            case PICK_CONTACT_REQUEST_CODE:
                Uri uri = data.getData();
                String[] projection = { ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,
                        null, null, null);
                cursor.moveToFirst();

                int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberColumnIndex);

                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = cursor.getString(nameColumnIndex);

                Contact contact = new Contact(name,number);
                currentContactQuestion.addContact(contact);

                TextView textView = new TextView(getActivity());
                textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                textView.setText(name +" - "+number);
                currentContactQuestion.getResultLine().addView(textView);
                break;
        }
    }
}
