package com.yellowpepper.unicomer.utils;

/**
 * Created by juang on 2/23/2018.
 */

public class Constants {
    public static final String TEXT ="text";
    public static final String RADIO = "radio";
    public static final String FILE = "file";
    public static final String CONTACT= "contact";
    public static final String LENGHT= "length";
    public static final String VALUE = "value";
    public static final String FORMAT = "format";
    public static final String OPTIONS = "options";
    public static final String URL = "url";
    public static final String SIZE = "size";
    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String OBJECT = "object";
    public static final String DATE = "date";
    public static final String NUMBER = "number";
    public static final String QUESTIONS = "questions";
    public static final String NUMERIC_REGEX = "^[1-9]\\d*(\\.\\d+)?$";
    public static final String DATE_FORMAT = "MM/dd/yyyy";
}
