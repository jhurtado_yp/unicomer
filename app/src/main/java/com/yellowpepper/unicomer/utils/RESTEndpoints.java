package com.yellowpepper.unicomer.utils;

import android.content.Context;

/**
 * Created by juang on 2/22/2018.
 */

public class RESTEndpoints {
private static Context appContext;
private static RESTEndpoints restEndpoints;

    private RESTEndpoints(Context context){
        appContext = context;
    }

    public static RESTEndpoints getInstance(Context context) {
        if (restEndpoints == null) {
            restEndpoints = new RESTEndpoints(context);
        }
        return restEndpoints;
    }

    public String mockQuestionsPath(){
        return "http://demo0740180.mockable.io/mockQuestion";
    }
}
