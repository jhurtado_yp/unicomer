package com.yellowpepper.unicomer.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by juang on 2/22/2018.
 */

public interface RestInvoker {

    <T extends Object> void receiveServerResponse(String requestTag, T jsonObject) throws JSONException;

    interface RestProcessor{
        void callRestAPI(RestInvoker invoker, String requestTag, String url, JSONObject jsonRequest, int requestMethod);
    }
}
